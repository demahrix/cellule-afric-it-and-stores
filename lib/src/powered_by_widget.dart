import 'package:flutter/material.dart';

class PoweredByWidget extends StatelessWidget {

  final bool center;

  PoweredByWidget({
    this.center: true
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Powered by ",
          style: TextStyle(
            fontFamily: "RobotoMono",
            package: "cellule_afric_it_and_stores"
          ),
        ),
        Text(
          "Afric It & Stores",
          style: TextStyle(
            fontFamily: "Nunito",
            fontWeight: FontWeight.w900,
            fontSize: 17.0,
            package: "cellule_afric_it_and_stores"
          ),
        )
      ],
    );
  }
}
